<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $invitation_id
 * @property integer $label_id
 * @property string $created_at
 * @property string $updated_at
 */
class InvitationLabel extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['invitation_id', 'label_id', 'created_at', 'updated_at'];

    public function labels() {
        return $this->hasOne(Label::class, 'id', 'label_id');
    }
}
