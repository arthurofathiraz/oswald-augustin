<?php

function response_error($message = "error", $errors = [])
{
    $result = [
        'message' => $message,
        'success' => false,
    ];

    if (count($errors) > 0) {
        $result['errors'] = $errors;
    }

    return $result;
}

function response_success($message = "success", $data = [])
{
    $result = [
        'message' => $message,
        'success' => true,
        'data' => $data,
    ];

    return $result;
}
