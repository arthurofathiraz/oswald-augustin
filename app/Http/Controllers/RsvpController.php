<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Models\Invitation;
use App\Models\InvitationLabel;
use App\Models\InvitationLog;
use App\Models\Label;
use App\Models\Rsvp;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RsvpController extends Controller
{
    public function rsvpView()
    {
        return view('pages/rsvp/rsvp', []);
    }

    public function rsvpAddView(Request $request)
    {
        $rsvp = [];

        return view('pages/rsvp/rsvp-add', [
            'rsvp' => $rsvp,
        ]);
    }

    public function rsvpAdd(Request $request)
    {
        $rule = [
            'nama' => 'required|min:1|max:200',
            'alamat' => 'required|min:1|max:200',
            'hadir' => 'required|in:ya,tidak',
            'jumlah' => 'required|min:1|max:8',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = Rsvp::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'hadir' => $request->hadir,
            'jumlah' => $request->jumlah,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data create"

            ), 200);
        }

        $rsvp = Rsvp::where('id', $update->id)->first();

        return response()->json(response_success(
            "success update data",
            $rsvp,
        ), 200);
    }

    public function rsvpEditView(Request $request, $id = 0)
    {
        $rsvp = Rsvp::where('id', $id)
            ->first();
        if (!$rsvp) {
            return redirect()
                ->route('invitation-rsvp-view')
                ->withErrors('data rsvp tidak di temukan');
        }

        return view('pages/rsvp/rsvp-edit', [
            'rsvp' => $rsvp,
            'id' => $id,
        ]);
    }

    public function rsvpEdit(Request $request, $id = 0)
    {
        $rule = [
            'nama' => 'required|min:1|max:200',
            'alamat' => 'required|min:1|max:200',
            'hadir' => 'required|in:ya,tidak',
            'jumlah' => 'required|min:1|max:8',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = Rsvp::where('id', $id)->first()->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'hadir' => $request->hadir,
            'jumlah' => $request->jumlah,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"

            ), 200);
        }

        $rsvp = Rsvp::where('id', $id)->first();

        return response()->json(response_success(
            "success update data",
            $rsvp,
        ), 200);
    }

    public function rsvpRemove(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // delete data
        $delete = Rsvp::where('id', $request->id)->first()->delete();
        if (!$delete) {
            return response()->json(response_error(
                "error remove data"
            ), 200);
        }

        return response()->json(response_success(
            "success remove data",
            [],
        ), 200);
    }

    public function rsvpGet(Request $request)
    {
        // set pagination limit
        $per_page = (!empty($request->get('size'))) ? $request->get('size') : 10;

        // set current page
        $page = (!empty($request->get('page'))) ? $request->get('page') : 1;

        // set offset
        $offset = 0;
        if ($per_page == "true") {
            $per_page = 999999999;
        } else {
            $offset = ($page - 1) * $per_page;
        }

        // set order by
        $order_by = (!empty($request->get('sorters'))) ? $request->get('sorters') : [
            [
                'field' => 'created_at',
                'dir' => 'asc'
            ],
        ];

        // set filters
        $filters = (!empty($request->get('filters'))) ? $request->get('filters') : [];

        // rsvp query
        $rsvp_query = Rsvp::query();

        // set sorters
        foreach ($order_by as $orderby) {
            $rsvp_query->orderBy($orderby['field'], $orderby['dir']);
        }

        // set filters
        foreach ($filters as $filter) {
            $rsvp_query->where($filter['field'], $filter['type'], "%" . $filter['value'] . "%");
        }

        // set result
        $total = $rsvp_query->count();
        if ($per_page == 999999999) {
            $rsvps = $rsvp_query
                ->get()
                ->toArray();

            $per_page = $total;
        } else {
            $rsvps = $rsvp_query->skip($offset)->take($per_page)
                ->get()
                ->toArray();
        }

        // set pagination
        $lastpage = 1;
        if ($total > 0) {
            $pagination = new LengthAwarePaginator($rsvps, $total, $per_page, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $lastpage = $pagination->lastPage();
        }

        return response()->json([
            'data' => $rsvps,
            'last_page' => $lastpage,
            'offset' => $offset,
            'per_page' => $per_page,
            'size' => $request->get('size'),
        ], 200);
    }
}
