<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Imports\InvitationsImport;
use App\Models\Configuration;
use App\Models\Invitation;
use App\Models\InvitationLabel;
use App\Models\InvitationLog;
use App\Models\Label;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\TextUI\Help;
use Maatwebsite\Excel\Excel as ExcelFormat;

class InvitationController extends Controller
{
    public function labelView()
    {
        return view('pages/invitation/label');
    }

    public function labelAddView(Request $request)
    {
        $configuration = Configuration::first();

        return view('pages/invitation/label-add', [
            'configuration' => $configuration
        ]);
    }

    public function labelAdd(Request $request)
    {
        $rule = [
            'name' => 'required|min:1|max:100',
            'description' => 'required|min:1|max:200',
            'priority' => 'required|numeric',
            'whatsapp_template' => 'required|min:1|max:5000',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // find priority
        $label = Label::where('priority', $request->priority)->first();
        if ($label) {
            return response()->json(response_error(
                "error validasi data",
                [
                    'priority' => 'Priority already taken'
                ],
            ), 200);
        }

        // update data
        $update = Label::create([
            'name' => $request->name,
            'description' => $request->description,
            'priority' => $request->priority,
            'whatsapp_template' => $request->whatsapp_template,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        return response()->json(response_success(
            "success update data",
            [],
        ), 200);
    }

    public function labelEditView(Request $request, $id = 0)
    {
        $label = Label::where('id', $id)->first();
        if (!$label) {
            return redirect()
                ->route('invitation-label-view')
                ->withErrors('data label tidak di temukan');
        }

        $configuration = Configuration::first();

        return view('pages/invitation/label-edit', [
            'label' => $label,
            'configuration' => $configuration,
        ]);
    }

    public function labelEdit(Request $request, $id = 0)
    {
        $rule = [
            'name' => 'required|min:1|max:100',
            'description' => 'required|min:1|max:200',
            'priority' => 'required|numeric',
            'whatsapp_template' => 'required|min:1|max:5000',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // find priority
        $label = Label::where('priority', $request->priority)->first();
        if ($label) {
            if ($label->id != $id) {
                return response()->json(response_error(
                    "error validasi data",
                    [
                        'priority' => 'Priority already taken'
                    ],
                ), 200);
            }
        }

        // update data
        $update = Label::where('id', $id)->first()->update([
            'name' => $request->name,
            'description' => $request->description,
            'priority' => $request->priority,
            'whatsapp_template' => $request->whatsapp_template,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        $label = Label::where('id', $id)->first();

        return response()->json(response_success(
            "success update data",
            $label,
        ), 200);
    }

    public function labelRemove(Request $request)
    {
        $rule = [
            'id' => 'required|exists:labels,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // delete data
        $delete = Label::where('id', $request->id)->first()->delete();
        if (!$delete) {
            return response()->json(response_error(
                "error remove data"
            ), 200);
        }

        return response()->json(response_success(
            "success remove data",
            [],
        ), 200);
    }

    public function labelGet(Request $request)
    {
        // set pagination limit
        $per_page = (!empty($request->get('size'))) ? $request->get('size') : 10;

        // set current page
        $page = (!empty($request->get('page'))) ? $request->get('page') : 1;

        // set offset
        $offset = ($page - 1) * $per_page;

        // set order by
        $order_by = (!empty($request->get('sorters'))) ? $request->get('sorters') : [
            [
                'field' => 'created_at',
                'dir' => 'asc'
            ],
            [
                'field' => 'priority',
                'dir' => 'asc'
            ],
        ];

        // set filters
        $filters = (!empty($request->get('filters'))) ? $request->get('filters') : [];

        // label query
        $label_query = Label::query();

        // set sorters
        foreach ($order_by as $orderby) {
            $label_query->orderBy($orderby['field'], $orderby['dir']);
        }

        // set filters
        foreach ($filters as $filter) {
            $label_query->where($filter['field'], $filter['type'], "%" . $filter['value'] . "%");
        }

        // set result
        $total = $label_query->count();
        $labels = $label_query->skip($offset)->take($per_page)
            ->get()
            ->toArray();

        // set pagination
        $lastpage = 1;
        if ($total > 0) {
            $pagination = new LengthAwarePaginator($labels, $total, $per_page, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $lastpage = $pagination->lastPage();
        }

        return response()->json([
            'data' => $labels,
            'last_page' => $lastpage,
        ], 200);
    }

    public function guestView()
    {
        $label = Label::all();

        return view('pages/invitation/guest', [
            'label' => $label,
        ]);
    }

    public function guestAddView(Request $request)
    {
        $guest = [];

        // generate qr_code_token
        $guest['qr_code_token'] = Hash::make(Str::random(20));

        // generate rsvp_code
        $guest['rsvp_code'] = Helper::generate_rsvp_code();

        $label = Label::all();
        if (!$label) {
            return redirect()
                ->route('invitation-guest-view')
                ->withErrors('data label tidak di temukan');
        }

        return view('pages/invitation/guest-add', [
            'guest' => $guest,
            'label' => $label,
        ]);
    }

    public function guestAdd(Request $request)
    {
        $rule = [
            'name' => 'required|min:1|max:100',
            'description' => 'required|min:1|max:200',
            'mobile' => 'required|min:1|max:20|unique:invitations,mobile|phone_number',
            'invitee_count' => 'required|min:1|max:8',
            'base' => 'required|min:1|max:200',
            'category' => 'required|in:MALE,FEMALE',
//            'status' => 'required|in:DRAFT,APPROVED,SENT,RSVP',
            'label' => 'required',
            'is_vip' => 'required|in:yes,no',
            'is_physic_invitation' => 'required|in:yes,no',
            'is_auto_rsvp' => 'required|in:yes,no',
            'qr_code_token' => 'required',
            'rsvp_code' => 'required',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // generate qrcode_file
        $faker = Faker::create('id');
        $random = md5($faker->name . Str::random(10));
        $nama_file = Helper::create_slug($faker->name) . "_" . $random . ".svg";
        Helper::generate_qr_file($request->name . '<|separator|>' . $request->mobile . '<|separator|>' . $request->qr_code_token, storage_path('app/public') . '/' . $nama_file);
        $qr_code_file = 'public/' . $nama_file;

        // update data
        $update = Invitation::create([
            'name' => $request->name,
            'description' => $request->description,
            'mobile' => $request->mobile,
            'invitee_count' => $request->invitee_count,
            'base' => $request->base,
            'category' => $request->category,
            'is_vip' => $request->is_vip == 'yes' ? true : false,
            'is_physic_invitation' => $request->is_physic_invitation == 'yes' ? true : false,
            'is_auto_rsvp' => $request->is_auto_rsvp == 'yes' ? true : false,
            'qr_code_token' => $request->qr_code_token,
            'rsvp_code' => $request->rsvp_code,
            'qr_code_file' => $qr_code_file,
            'status' => 'DRAFT',
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data create"

            ), 200);
        }

        // set invitation log
        $update_log = InvitationLog::create([
            'user_id' => auth()->user()->id,
            'invitation_id' => $update->id,
            'status' => 'DRAFT',
            'remarks' => '',
        ]);
        if (!$update_log) {
            return response()->json(response_error(
                "error update data create"

            ), 200);
        }

        // set label
        InvitationLabel::where('invitation_id', $update->id)->delete();
        if (count($request->label) > 0) {
            foreach ($request->label as $label) {
                $update_label = InvitationLabel::create([
                    'invitation_id' => $update->id,
                    'label_id' => $label,
                ]);
                if (!$update_label) {
                    return response()->json(response_error(
                        "error update data add label"

                    ), 200);
                }
            }
        }

        $guest = Invitation::where('id', $update->id)->first();

        return response()->json(response_success(
            "success update data",
            $guest,
        ), 200);
    }

    public function guestEditView(Request $request, $id = 0)
    {
        $guest = Invitation::where('id', $id)
            ->with('invitation_labels.labels')
            ->first();
        if (!$guest) {
            return redirect()
                ->route('invitation-guest-view')
                ->withErrors('data guest tidak di temukan');
        }

        // generate qr_code_token
        if (empty($guest->qr_code_token)) {
            $guest->qr_code_token = Hash::make(Str::random(20));
        }

        // generate rsvp_code
        if (empty($guest->rsvp_code)) {
            $guest->rsvp_code = Helper::generate_rsvp_code();
        }

        $label = Label::all();
        if (!$label) {
            return redirect()
                ->route('invitation-guest-view')
                ->withErrors('data label tidak di temukan');
        }

        return view('pages/invitation/guest-edit', [
            'guest' => $guest,
            'label' => $label,
            'id' => $id,
        ]);
    }

    public function guestEdit(Request $request, $id = 0)
    {
        $rule = [
            'name' => 'required|min:1|max:100',
            'description' => 'required|min:1|max:200',
            'mobile' => 'required|min:1|max:20|phone_number',
            'invitee_count' => 'required|min:1|max:8',
            'base' => 'required|min:1|max:200',
            'category' => 'required|in:MALE,FEMALE',
//            'status' => 'required|in:DRAFT,APPROVED,SENT,RSVP',
            'label' => 'required',
            'is_vip' => 'required|in:yes,no',
            'is_physic_invitation' => 'required|in:yes,no',
            'is_auto_rsvp' => 'required|in:yes,no',
            'qr_code_token' => 'required',
            'rsvp_code' => 'required',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // generate qrcode_file
        $faker = Faker::create('id');
        $random = md5($faker->name . Str::random(10));
        $nama_file = Helper::create_slug($faker->name) . "_" . $random . ".svg";
        Helper::generate_qr_file($request->name . '<|separator|>' . $request->mobile . '<|separator|>' . $request->qr_code_token, storage_path('app/public') . '/' . $nama_file);
        $qr_code_file = 'public/' . $nama_file;

        // update data
        $update = Invitation::where('id', $id)->first()->update([
            'name' => $request->name,
            'description' => $request->description,
            'mobile' => $request->mobile,
            'invitee_count' => $request->invitee_count,
            'base' => $request->base,
            'category' => $request->category,
            'is_vip' => $request->is_vip == 'yes' ? true : false,
            'is_physic_invitation' => $request->is_physic_invitation == 'yes' ? true : false,
            'is_auto_rsvp' => $request->is_auto_rsvp == 'yes' ? true : false,
            'qr_code_token' => $request->qr_code_token,
            'rsvp_code' => $request->rsvp_code,
            'qr_code_file' => $qr_code_file,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"

            ), 200);
        }

        // set label
        InvitationLabel::where('invitation_id', $id)->delete();
        if (count($request->label) > 0) {
            foreach ($request->label as $label) {
                $update_label = InvitationLabel::create([
                    'invitation_id' => $id,
                    'label_id' => $label,
                ]);
                if (!$update_label) {
                    return response()->json(response_error(
                        "error update data"

                    ), 200);
                }
            }
        }

        $guest = Invitation::where('id', $id)->first();

        return response()->json(response_success(
            "success update data",
            $guest,
        ), 200);
    }

    public function guestRemove(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // delete data
        $delete = Invitation::where('id', $request->id)->first()->delete();
        if (!$delete) {
            return response()->json(response_error(
                "error remove data"
            ), 200);
        }

        InvitationLabel::where('invitation_id', $request->id)->delete();
        InvitationLog::where('invitation_id', $request->id)->delete();

        return response()->json(response_success(
            "success remove data",
            [],
        ), 200);
    }

    public function guestApprove(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // delete data
        $delete = Invitation::where('id', $request->id)->first()->update([
            'status' => 'APPROVED'
        ]);
        if (!$delete) {
            return response()->json(response_error(
                "error approve data"
            ), 200);
        }

        // set invitation log
        $update_log = InvitationLog::create([
            'user_id' => auth()->user()->id,
            'invitation_id' => $request->id,
            'status' => 'APPROVED',
            'remarks' => '',
        ]);
        if (!$update_log) {
            return response()->json(response_error(
                "error approve data"
            ), 200);
        }

        return response()->json(response_success(
            "success approve data",
            [],
        ), 200);
    }

    public function guestBulkApprove(Request $request)
    {
        $rule = [
            'id' => 'required',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // split by comma
        $ids = explode(",", $request->id);
        foreach ($ids as $id) {
            // delete data
            $delete = Invitation::where('id', $id)->first()->update([
                'status' => 'APPROVED'
            ]);
            if (!$delete) {
                return response()->json(response_error(
                    "error approve data"
                ), 200);
            }

            // set invitation log
            $update_log = InvitationLog::create([
                'user_id' => auth()->user()->id,
                'invitation_id' => $id,
                'status' => 'APPROVED',
                'remarks' => '',
            ]);
            if (!$update_log) {
                return response()->json(response_error(
                    "error approve data"
                ), 200);
            }
        }

        return response()->json(response_success(
            "success approve data",
            [],
        ), 200);
    }

    public function guestSendPreview(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
            'mobile' => 'required|phone_number',
            'is_whatsapp_maps' => 'required|in:yes,no',
            'is_whatsapp_qr' => 'required|in:yes,no',
            'is_whatsapp_rsvp' => 'required|in:yes,no',
            'label_id' => 'required|exists:labels,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get configuration
        $configuration = Configuration::first();
        if (!$configuration) {
            return response()->json(response_error(
                "error send preview data"
            ), 200);
        }

        // get invitation data
        $invitation = Invitation::where('id', $request->id)
            ->with('invitation_labels.labels')
            ->first();
        if (!$invitation) {
            return response()->json(response_error(
                "error send preview data"
            ), 200);
        }

        // get label
        $priority_label = Label::where('id', $request->label_id)->first();
        if (!$priority_label) {
            return response()->json(response_error(
                "error send preview data"
            ), 200);
        }

        // rsvp url
        $rsvp_url = $invitation->is_auto_rsvp ? "&rsvp=false" : "";

        // get lowest priority label
        $message = $priority_label->whatsapp_template;

        // replace data
        $message = preg_replace(
            [
                '/{invitation_name}/',
                '/{invitation_name_url}/',
                '/{male_name}/',
                '/{male_description}/',
                '/{female_name}/',
                '/{female_description}/',
                '/\n/',
            ],
            [
                $invitation->name,
                urlencode($invitation->name) . $rsvp_url,
                $configuration->male_name,
                $configuration->male_description,
                $configuration->female_name,
                $configuration->female_description,
                "\r\n",
            ],
            $message
        );

        // send to whatsapp api message
        $response = Helper::send_whatsapp(
            env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
            [
                'message' => $message,
            ],
        );
        if (!$response['success']) {
            return response()->json(response_error(
                "error send preview data " . $response['messsage'] . json_encode($response)
            ), 200);
        }


        // send streaming whatsapp api
        if ($priority_label->name == 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_streaming_message($configuration->zoom_live_url, $configuration->meet_live_url, $configuration->instagram_live_id),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send preview data " . $response['messsage']
                ), 200);
            }
        }

        // send rsvp code whatsapp api
        if ($request->is_whatsapp_rsvp == 'yes' && $priority_label->name != 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_rsvp_message($invitation->name, $invitation->rsvp_code, $invitation->mobile, $invitation->is_auto_rsvp),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send preview data " . $response['messsage']
                ), 200);
            }
        }

        // send to whatsapp api qrcode media
        if ($request->is_whatsapp_qr == 'yes' && $priority_label->name != 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_qrcode_message(),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send preview data " . $response['messsage']
                ), 200);
            }

            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/media',
                Helper::get_qrcode_image($invitation->qr_code_file),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send preview data " . $response['messsage']
                ), 200);
            }
        }

        // send to whatsapp api location
        if ($request->is_whatsapp_maps == 'yes' && $priority_label->name != 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_location_message(),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send preview data " . $response['messsage']
                ), 200);
            }
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/location',
                Helper::get_location_point($configuration->walimah_latitude, $configuration->walimah_longitude, $configuration->walimah_name),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send preview data " . $response['messsage']
                ), 200);
            }
        }

        return response()->json(response_success(
            "success send preview data",
            [
                'message' => $message,
            ],
        ), 200);
    }

    public function guestSend(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
            'mobile' => 'required|phone_number',
            'is_whatsapp_maps' => 'required|in:yes,no',
            'is_whatsapp_qr' => 'required|in:yes,no',
            'is_whatsapp_rsvp' => 'required|in:yes,no',
            'label_id' => 'required|exists:labels,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get configuration
        $configuration = Configuration::first();
        if (!$configuration) {
            return response()->json(response_error(
                "error send whatsapp data"
            ), 200);
        }

        // get invitation data
        $invitation = Invitation::where('id', $request->id)
            ->with('invitation_labels.labels')
            ->first();
        if (!$invitation) {
            return response()->json(response_error(
                "error send whatsapp data"
            ), 200);
        }

        // get label
        $priority_label = Label::where('id', $request->label_id)->first();
        if (!$priority_label) {
            return response()->json(response_error(
                "error send whatsapp data"
            ), 200);
        }

        // rsvp url
        $rsvp_url = $invitation->is_auto_rsvp ? "&rsvp=false" : "";

        // get lowest priority label
        $message = $priority_label->whatsapp_template;

        // replace data
        $message = preg_replace(
            [
                '/{invitation_name}/',
                '/{invitation_name_url}/',
                '/{male_name}/',
                '/{male_description}/',
                '/{female_name}/',
                '/{female_description}/',
                '/\n/',
            ],
            [
                $invitation->name,
                urlencode($invitation->name) . $rsvp_url,
                $configuration->male_name,
                $configuration->male_description,
                $configuration->female_name,
                $configuration->female_description,
                "\r\n",
            ],
            $message
        );

        // send to whatsapp api message
        $response = Helper::send_whatsapp(
            env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
            [
                'message' => $message,
            ],
        );
        if (!$response['success']) {
            return response()->json(response_error(
                "error send whatsapp data " . $response['messsage']
            ), 200);
        }


        // send streaming whatsapp api
        if ($priority_label->name == 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_streaming_message($configuration->zoom_live_url, $configuration->meet_live_url, $configuration->instagram_live_id),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send whatsapp data " . $response['messsage']
                ), 200);
            }
        }

        // send rsvp code whatsapp api
        if ($request->is_whatsapp_rsvp == 'yes' && $priority_label->name != 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_rsvp_message($invitation->name, $invitation->rsvp_code, $invitation->mobile, $invitation->is_auto_rsvp),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send whatsapp data " . $response['messsage']
                ), 200);
            }
        }

        // send to whatsapp api qrcode media
        if ($request->is_whatsapp_qr == 'yes' && $priority_label->name != 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_qrcode_message(),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send whatsapp data " . $response['messsage']
                ), 200);
            }

            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/media',
                Helper::get_qrcode_image($invitation->qr_code_file),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send whatsapp data " . $response['messsage']
                ), 200);
            }
        }

        // send to whatsapp api location
        if ($request->is_whatsapp_maps == 'yes' && $priority_label->name != 'streaming') {
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                Helper::get_location_message(),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send whatsapp data " . $response['messsage']
                ), 200);
            }
            $response = Helper::send_whatsapp(
                env('WHATSAPP_URL') . '/message/' . $request->mobile . '/location',
                Helper::get_location_point($configuration->walimah_latitude, $configuration->walimah_longitude, $configuration->walimah_name),
            );
            if (!$response['success']) {
                return response()->json(response_error(
                    "error send whatsapp data " . $response['messsage']
                ), 200);
            }
        }

        // set status to SENT/RSVP
        $status = ($invitation->is_auto_rsvp) ? 'RSVP' : 'SENT';
        $update = $invitation->update(['status' => $status]);
        if (!$update) {
            return response()->json(response_error(
                "error send whatsapp data"
            ), 200);
        }

        // set invitation log
        $update_log = InvitationLog::create([
            'user_id' => auth()->user()->id,
            'invitation_id' => $request->id,
            'status' => $status,
            'remarks' => '',
        ]);
        if (!$update_log) {
            return response()->json(response_error(
                "error send whatsapp data"
            ), 200);
        }

        return response()->json(response_success(
            "success send whatsapp data",
            [
                'message' => $message,
            ],
        ), 200);
    }

    public function guestSendReminderPreview(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
            'mobile' => 'required|phone_number',
            'type' => 'required|in:h_7_sahabat,h_7_ortu,h_2_sahabat,h_2_ortu,sesi_1_sahabat,sesi_1_ortu,sesi_2_sahabat,sesi_2_ortu,guide_sahabat,guide_ortu,qr_code_sahabat,qr_code_ortu,maps_sahabat,maps_ortu',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get configuration
        $configuration = Configuration::first();
        if (!$configuration) {
            return response()->json(response_error(
                "error send reminder preview data"
            ), 200);
        }

        // get invitation data
        $invitation = Invitation::where('id', $request->id)
            ->first();
        if (!$invitation) {
            return response()->json(response_error(
                "error send reminder preview data"
            ), 200);
        }

        // check if ortu
        $is_ortu = Helper::check_is_ortu($request->type);

        // switch action
        switch ($request->type) {
            case "h_2_sahabat":
            case "h_2_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_h2_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "h_7_sahabat":
            case "h_7_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_h7_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "sesi_1_sahabat":
            case "sesi_1_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_sesi1_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "sesi_2_sahabat":
            case "sesi_2_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_sesi2_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "guide_sahabat":
            case "guide_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_guide_message(
                        $invitation,
                        $configuration
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }

                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/media',
                    Helper::get_guide_image(
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                break;
            case "qr_code_sahabat":
            case "qr_code_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_qrcode_message(),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }

                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/media',
                    Helper::get_qrcode_image($invitation->qr_code_file),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                break;
            case "maps_sahabat":
            case "maps_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_location_message(),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/location',
                    Helper::get_location_point($configuration->walimah_latitude, $configuration->walimah_longitude, $configuration->walimah_name),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                break;
        }

        return response()->json(response_success(
            "success send reminder preview data",
            [],
        ), 200);
    }

    public function guestSendReminder(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
            'mobile' => 'required|phone_number',
            'type' => 'required|in:h_7_sahabat,h_7_ortu,h_2_sahabat,h_2_ortu,sesi_1_sahabat,sesi_1_ortu,sesi_2_sahabat,sesi_2_ortu,guide_sahabat,guide_ortu,qr_code_sahabat,qr_code_ortu,maps_sahabat,maps_ortu',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get configuration
        $configuration = Configuration::first();
        if (!$configuration) {
            return response()->json(response_error(
                "error send reminder preview data"
            ), 200);
        }

        // get invitation data
        $invitation = Invitation::where('id', $request->id)
            ->first();
        if (!$invitation) {
            return response()->json(response_error(
                "error send reminder preview data"
            ), 200);
        }

        // check if ortu
        $is_ortu = Helper::check_is_ortu($request->type);

        // switch action
        switch ($request->type) {
            case "h_2_sahabat":
            case "h_2_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_h2_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "h_7_sahabat":
            case "h_7_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_h7_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "sesi_1_sahabat":
            case "sesi_1_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_sesi1_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "sesi_2_sahabat":
            case "sesi_2_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_sesi2_message(
                        $invitation,
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }
                break;
            case "guide_sahabat":
            case "guide_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_guide_message(
                        $invitation,
                        $configuration
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage'] . json_encode($response)
                    ), 200);
                }

                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/media',
                    Helper::get_guide_image(
                        $configuration,
                        $is_ortu
                    ),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                break;
            case "qr_code_sahabat":
            case "qr_code_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_qrcode_message(),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }

                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/media',
                    Helper::get_qrcode_image($invitation->qr_code_file),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                break;
            case "maps_sahabat":
            case "maps_ortu":
                // send to whatsapp api message
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/string',
                    Helper::get_location_message(),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                $response = Helper::send_whatsapp(
                    env('WHATSAPP_URL') . '/message/' . $request->mobile . '/location',
                    Helper::get_location_point($configuration->walimah_latitude, $configuration->walimah_longitude, $configuration->walimah_name),
                );
                if (!$response['success']) {
                    return response()->json(response_error(
                        "error send reminder preview data " . $response['messsage']
                    ), 200);
                }
                break;
        }

        return response()->json(response_success(
            "success send reminder preview data",
            [],
        ), 200);
    }

    public function guestImport(Request $request)
    {
        $rule = [
            'file' => 'required',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // set file
        $file = $request->file('file');

        // set file extension
        $file_extension = strtolower($file->getClientOriginalExtension());

        // set path and name
        $file_path = public_path();
        $file_name = 'temp.' . $file_extension;
        $file_path_name = $file_path . '/' . $file_name;

        // move to temp file
        $file->move($file_path, $file_name);

        $import = Excel::toArray(new InvitationsImport, $file_path_name, null, ExcelFormat::CSV);

        // set data import
        $data = [];
        $no = 1;
        $line = 1;

        // looping each value
        foreach ($import as $key => $value) {

            // loop each row
            for ($i = 1, $iMax = count($value); $i < $iMax; $i++) {

                // set row value | from the library it holds in array 0.
                $column = $value[$i];

                // check for column where not empty
                if (count($column) > 0) {

                    $name = (isset($column[0]) && $column[0] != "") ? $column[0] : "";
                    $description = (isset($column[1]) && $column[1] != "") ? $column[1] : "";
                    $mobile = (isset($column[2]) && $column[2] != "") ? $column[2] : "";
                    $invitee_count = (isset($column[3]) && $column[3] != "") ? $column[3] : "";
                    $base = (isset($column[4]) && $column[4] != "") ? $column[4] : "";
                    $category = (isset($column[5]) && $column[5] != "") ? $column[5] : "";
                    $label = (isset($column[6]) && $column[6] != "") ? $column[6] : "";
                    $is_vip = (isset($column[7]) && $column[7] != "") ? $column[7] : "";
                    $is_physic_invitation = (isset($column[8]) && $column[8] != "") ? $column[8] : "";
                    $is_auto_rsvp = (isset($column[9]) && $column[9] != "") ? $column[9] : "";

                    $data[] = [
                        'no' => $line,
                        'name' => $name,
                        'description' => $description,
                        'mobile' => $mobile,
                        'invitee_count' => $invitee_count,
                        'base' => $base,
                        'category' => $category,
                        'label' => $label,
                        'is_vip' => $is_vip,
                        'is_physic_invitation' => $is_physic_invitation,
                        'is_auto_rsvp' => $is_auto_rsvp,
                    ];

                    $line++;
                }
                $no++;
            }
        }

        // delete file
        File::delete($file_path_name);

        return response()->json(response_success(
            "success import data",
            $data,
        ), 200);
    }

    public function guestImportTable(Request $request)
    {
        $rule = [
            'name.*' => 'required|min:1|max:100',
            'description.*' => 'nullable|min:1|max:200',
            'mobile.*' => 'required|min:1|max:20|phone_number',
            'invitee_count.*' => 'required|min:1|max:8',
            'base.*' => 'required|min:1|max:200',
            'category.*' => 'required|in:MALE,FEMALE',
            'label.*' => 'required',
            'is_vip.*' => 'required|in:yes,no',
            'is_physic_invitation.*' => 'required|in:yes,no',
            'is_auto_rsvp.*' => 'required|in:yes,no',
            'qr_code_token.*' => 'required',
            'rsvp_code.*' => 'required',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get all labels
        $label_data = [];
        $label_all = Label::all();
        foreach ($label_all as $lbl_all) {
            $label_data[$lbl_all->name] = $lbl_all;
        }

        // loop each data
        foreach ($request->mobile as $index => $value) {

            // default data
            $status = 'DRAFT';
            $is_generate_qr = true;
            $is_insert = true;
            $is_set_log = true;
            $qr_code_file = '';
            $qr_code_token = Hash::make(Str::random(20));
            $rsvp_code = Helper::generate_rsvp_code();
            $id = 0;

            // find by mobile number
            $invitation = Invitation::where('mobile', $request->mobile[$index])->first();
            if ($invitation) {

                // update
                $id = $invitation->id;
                $status = $invitation->status;
                $is_insert = false;
                $is_set_log = false;

                // check qrcode_file
                if (!empty($invitation->qr_code_file)) {
                    $is_generate_qr = false;
                }

                // check qr_code_token
                if (!empty($guest->qr_code_token)) {
                    $qr_code_token = $qr_code_token->qr_code_token;
                }

                // check rsvp_code
                if (!empty($guest->rsvp_code)) {
                    $rsvp_code = $rsvp_code->rsvp_code;
                }
            }

            // generate qrcode_file
            if ($is_generate_qr) {
                $faker = Faker::create('id');
                $random = md5($faker->name . Str::random(10));
                $nama_file = Helper::create_slug($faker->name) . "_" . $random . ".svg";
                Helper::generate_qr_file($request->name[$index] . '<|separator|>' . $request->mobile[$index] . '<|separator|>' . $qr_code_token, storage_path('app/public') . '/' . $nama_file);
                $qr_code_file = 'public/' . $nama_file;
            }

            $data = [
                'name' => $request->name[$index],
                'description' => $request->description[$index],
                'mobile' => $request->mobile[$index],
                'invitee_count' => $request->invitee_count[$index],
                'base' => $request->base[$index],
                'category' => $request->category[$index],
                'is_vip' => $request->is_vip[$index] == 'yes' ? true : false,
                'is_physic_invitation' => $request->is_physic_invitation[$index] == 'yes' ? true : false,
                'is_auto_rsvp' => $request->is_auto_rsvp[$index] == 'yes' ? true : false,
                'qr_code_token' => $qr_code_token,
                'rsvp_code' => $rsvp_code,
                'qr_code_file' => $qr_code_file,
                'status' => $status,
            ];

            // check insert
            if ($is_insert) {
                $update = Invitation::create($data);
                if (!$update) {
                    return response()->json(response_error(
                        "error update data"
                    ), 200);
                }

                // set id
                $id = $update->id;
            } else {
                $update = $invitation->update($data);
                if (!$update) {
                    return response()->json(response_error(
                        "error update data"
                    ), 200);
                }
            }

            // set label
            InvitationLabel::where('invitation_id', $id)->delete();
            $labels = explode(",", $request->label[$index]);
            if (count($labels) > 0) {
                foreach ($labels as $label) {
                    if ($label_data[$label]) {
                        $update_label = InvitationLabel::create([
                            'invitation_id' => $id,
                            'label_id' => $label_data[$label]['id'],
                        ]);
                        if (!$update_label) {
                            return response()->json(response_error(
                                "error update data"
                            ), 200);
                        }
                    }
                }
            }

            // check log
            if ($is_set_log) {
                $update_log = InvitationLog::create([
                    'user_id' => auth()->user()->id,
                    'invitation_id' => $id,
                    'status' => $status,
                    'remarks' => '',
                ]);
                if (!$update_log) {
                    return response()->json(response_error(
                        "error update data"

                    ), 200);
                }
            }
        }

        return response()->json(response_success(
            "success import table data",
            [],
        ), 200);
    }

    public function guestChangeStatus(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
            'status' => 'required|in:APPROVED,DRAFT,RSVP,CHECKED_IN,SENT',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = Invitation::where('id', $request->id)->first()->update([
            'status' => $request->status,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error change status data"
            ), 200);
        }

        // set invitation log
        $update_log = InvitationLog::create([
            'user_id' => auth()->user()->id,
            'invitation_id' => $request->id,
            'status' => $request->status,
            'remarks' => '',
        ]);
        if (!$update_log) {
            return response()->json(response_error(
                "error change status data"
            ), 200);
        }

        return response()->json(response_success(
            "success change status data",
            [],
        ), 200);
    }

    public function guestGet(Request $request)
    {
        // set pagination limit
        $per_page = (!empty($request->get('size'))) ? $request->get('size') : 10;

        // set current page
        $page = (!empty($request->get('page'))) ? $request->get('page') : 1;

        // set offset
        $offset = 0;
        if ($per_page == "true") {
            $per_page = 999999999;
        } else {
            $offset = ($page - 1) * $per_page;
        }

        // set order by
        $order_by = (!empty($request->get('sorters'))) ? $request->get('sorters') : [
            [
                'field' => 'created_at',
                'dir' => 'asc'
            ],
        ];

        // set filters
        $filters = (!empty($request->get('filters'))) ? $request->get('filters') : [];

        // guest query
        $guest_query = Invitation::query()
            ->with('invitation_labels.labels');

        // set sorters
        foreach ($order_by as $orderby) {
            if ($orderby['field'] == 'invitation_labels') {
                $guest_query->whereHas('invitation_labels', function ($queryInvitationLabels) use ($orderby) {
                    $queryInvitationLabels->whereHas('labels', function ($queryLabels) use ($orderby) {
                        $queryLabels->orderBy('name', $orderby['dir']);
                    });
                });
            } else {
                $guest_query->orderBy($orderby['field'], $orderby['dir']);
            }
        }

        // set filters
        foreach ($filters as $filter) {
            if ($filter['field'] == 'label') {
                $guest_query->whereHas('invitation_labels', function ($queryInvitationLabels) use ($filter) {
                    $queryInvitationLabels->whereHas('labels', function ($queryLabels) use ($filter) {
                        $queryLabels->where('name', $filter['type'], "%" . $filter['value'] . "%");
                    });
                });
            } else {
                $guest_query->where($filter['field'], $filter['type'], "%" . $filter['value'] . "%");
            }
        }

        // set result
        $total = $guest_query->count();
        if ($per_page == 999999999) {
            $guests = $guest_query
                ->get()
                ->toArray();

            $per_page = $total;
        } else {
            $guests = $guest_query->skip($offset)->take($per_page)
                ->get()
                ->toArray();
        }

        // loop to set data for print
        $index = 0;
        foreach ($guests as $guest) {
            $guest['is_vip_download'] = ($guest['is_vip']) ? 'yes' : 'no';
            $guest['is_physic_invitation_download'] = ($guest['is_physic_invitation']) ? 'yes' : 'no';
            $guest['is_auto_rsvp_download'] = ($guest['is_auto_rsvp']) ? 'yes' : 'no';

            $guest_invitation = [];
            foreach ($guest['invitation_labels'] as $invitation_label) {
                $guest_invitation[] = $invitation_label['labels']['name'];
            }
            $guest['label_download'] = (count($guest_invitation) > 0) ? implode(",", $guest_invitation) : '';

            // set priority label
            $guest['priority_labels'] = Helper::get_priority_label($guest);
            $guest['is_whatsapp_maps'] = ($guest['priority_labels']['name'] != 'streaming' && $guest['status'] == 'RSVP') ? 'yes' : 'no';

            // set only qr for physic invitation
            $guest['is_whatsapp_qr'] = 'no';
            $guest['is_whatsapp_rsvp'] = 'yes';
            if ($guest['priority_labels']['name'] != 'streaming') {
                if ($guest['is_auto_rsvp']) {
                    $guest['is_whatsapp_qr'] = 'yes';
                    $guest['is_whatsapp_rsvp'] = 'no';
                }
            }

            // set new guest
            $guests[$index] = $guest;

            $index++;
        }

        // set pagination
        $lastpage = 1;
        if ($total > 0) {
            $pagination = new LengthAwarePaginator($guests, $total, $per_page, $page, ['path' => $request->url(), 'query' => $request->query()]);
            $lastpage = $pagination->lastPage();
        }

        return response()->json([
            'data' => $guests,
            'last_page' => $lastpage,
            'offset' => $offset,
            'per_page' => $per_page,
            'size' => $request->get('size'),
        ], 200);
    }

    public
    function checkinView()
    {
        $invitation = Invitation::orderBy('name', 'asc')->get();
        $configuration = Configuration::first();

        return view('pages/invitation/guest-checkin',
            [
                'invitation' => $invitation,
                'configuration' => $configuration,
            ]
        );
    }

    public
    function checkin(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                $validate->messages()->all(),
                $validate->errors(),
            ), 200);
        }

        // get data
        $data = Invitation::where('id', $request->id)->first();
        if (!$data) {
            return response()->json(response_error(
                "error get checked in data"
            ), 200);
        }

        // validate status
        if ($data->status == 'CHECKED_IN') {
            return response()->json(response_error(
                "error anda sudah melakukan check in sebelumnya"
            ), 200);
        }

        if ($data->status != 'RSVP') {
            return response()->json(response_error(
                "error hanya bisa untuk status RSVP"
            ), 200);
        }

        // delete data
        $delete = Invitation::where('id', $request->id)->first()->update([
            'status' => 'CHECKED_IN',
            'checked_in_at' => Carbon::now(),
        ]);
        if (!$delete) {
            return response()->json(response_error(
                "error checked in data"
            ), 200);
        }

        // set invitation log
        $update_log = InvitationLog::create([
            'user_id' => auth()->user()->id,
            'invitation_id' => $request->id,
            'status' => 'CHECKED_IN',
            'remarks' => '',
        ]);
        if (!$update_log) {
            return response()->json(response_error(
                "error checked in data"
            ), 200);
        }

        return response()->json(response_success(
            "success checked in data",
            [],
        ), 200);
    }

    public
    function checkinQrcode(Request $request)
    {
        $rule = [
            'id' => 'required|exists:invitations,id',
            'mobile' => 'required|exists:invitations,mobile',
            'qr_code_token' => 'required|exists:invitations,qr_code_token',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                $validate->messages()->all(),
                $validate->errors(),
            ), 200);
        }

        // get data
        $data = Invitation::where('id', $request->id)->first();
        if (!$data) {
            return response()->json(response_error(
                "error get checked in data"
            ), 200);
        }

        // validate status
        if ($data->status == 'CHECKED_IN') {
            return response()->json(response_error(
                "error anda sudah melakukan check in sebelumnya"
            ), 200);
        }

        if ($data->status != 'RSVP') {
            return response()->json(response_error(
                "error hanya bisa untuk status RSVP"
            ), 200);
        }

        // validate token
        if ($data->qr_code_token != $request->qr_code_token) {
            return response()->json(response_error(
                "error rsvp token tidak valid"
            ), 200);
        }

        // delete data
        $delete = Invitation::where('id', $request->id)->first()->update([
            'status' => 'CHECKED_IN',
            'checked_in_at' => Carbon::now(),
        ]);
        if (!$delete) {
            return response()->json(response_error(
                "error checked in data"
            ), 200);
        }

        // set invitation log
        $update_log = InvitationLog::create([
            'user_id' => auth()->user()->id,
            'invitation_id' => $request->id,
            'status' => 'CHECKED_IN',
            'remarks' => '',
        ]);
        if (!$update_log) {
            return response()->json(response_error(
                "error checked in data"
            ), 200);
        }

        return response()->json(response_success(
            "success checked in data",
            [],
        ), 200);
    }

    public
    function checkinQrcodeGet(Request $request, $mobile)
    {
        // get data
        $get = Invitation::where('mobile', $mobile)->first();
        if (!$get) {
            return response()->json(response_error(
                "error get check in data"
            ), 200);
        }

        return response()->json(response_success(
            "success checked in data",
            $get,
        ), 200);
    }

}
