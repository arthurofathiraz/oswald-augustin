<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'configuration-guide-upload',
        'configuration-guide-old-upload',
        'configuration-invitation-upload',
        'configuration-qris-upload',
        'configuration-song-upload',
        'configuration-protocol-upload',
        'configuration-male-upload',
        'configuration-female-upload',
        'configuration-gallery-upload',
        'invitation-guest-import',
    ];
}
