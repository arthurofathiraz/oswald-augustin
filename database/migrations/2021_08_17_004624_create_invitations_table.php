<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('status')->default('DRAFT'); // DRAFT (pertama kali di upload lewat excel
                                                                     // APPROVED (ketika data sudah valid, setelah klik tombol submit)
                                                                     // SENT (undangan dikirim)
                                                                     // RSVP (ketika sudah melakukan registrasi, untuk keluarga auto set RSVP)
            $table->integer('invitee_count')->default(1);
            $table->string('base')->nullable();
            $table->boolean('is_accomodation')->default(false);
            $table->text('accomodation_info')->nullable();
            $table->boolean('is_vip')->default(false);
            $table->boolean('is_physic_invitation')->default(false);
            $table->boolean('is_auto_rsvp')->default(false);
            $table->string('mobile')->nullable();
            $table->string('rsvp_code')->nullable();
            $table->text('qr_code_file')->nullable();
            $table->text('qr_code_token')->nullable();
            $table->text('category')->nullable(); // MALE / FEMALE
            $table->timestamp('rsvp_at')->nullable();
            $table->timestamp('checked_in_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
